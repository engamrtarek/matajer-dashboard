<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', 'Dashboard\AdminsController@login_form')->name('login');
Route::post('/admin/login', 'Dashboard\AdminsController@login')->name('admins.login');
Route::get('/logout', 'Dashboard\AdminsController@logout')->name('logout');
Route::get('/profile', 'Dashboard\AdminsController@profileIndex')->name('admins.profile');
Route::post('/update-profile/{id}', 'Dashboard\AdminsController@updateProfile')->name('admins.profile.update');
Route::group(['prefix' => 'admin'], function () {
    Route::group(['middleware' => 'auth:admin'], function () {
        Route::group(['middleware' => ['role:Super Admin|Settings Admin']], function () {
            Route::resource('dashboard', 'Dashboard\StatisticsController')->only('index');
            Route::prefix('cities')->group(function() {
                Route::get('','Dashboard\CityController@index')->name('cities.index');
                Route::get('/create','Dashboard\CityController@create')->name('cities.create');
                Route::post('/store','Dashboard\CityController@store')->name('cities.store');
                Route::get('/edit/{id}','Dashboard\CityController@edit')->name('cities.edit');
                Route::post('/update/{id}','Dashboard\CityController@update')->name('cities.update');
                Route::post('/{id}','Dashboard\CityController@destroy')->name('cities.destroy');
            });

            Route::prefix('neighborhoods')->group(function() {
                Route::get('','Dashboard\NeighborhoodsController@index')->name('neighborhoods.index');
                Route::get('/create','Dashboard\NeighborhoodsController@create')->name('neighborhoods.create');
                Route::post('/store','Dashboard\NeighborhoodsController@store')->name('neighborhoods.store');
                Route::get('/edit/{id}','Dashboard\NeighborhoodsController@edit')->name('neighborhoods.edit');
                Route::post('/update/{id}','Dashboard\NeighborhoodsController@update')->name('neighborhoods.update');
                Route::post('/{id}','Dashboard\NeighborhoodsController@destroy')->name('neighborhoods.destroy');
            });

            Route::prefix('categories')->group(function() {
                Route::get('','Dashboard\CategoryController@index')->name('categories.index');
                Route::get('/create','Dashboard\CategoryController@create')->name('categories.create');
                Route::post('/store','Dashboard\CategoryController@store')->name('categories.store');
                Route::get('/edit/{id}','Dashboard\CategoryController@edit')->name('categories.edit');
                Route::post('/update/{id}','Dashboard\CategoryController@update')->name('categories.update');
                Route::post('/{id}','Dashboard\CategoryController@destroy')->name('categories.destroy');
                Route::post('/status/{id}', 'Dashboard\CategoryController@updateStatus')->name('categories.status');
            });

            Route::prefix('additions')->group(function() {
                Route::get('','Dashboard\AdditionController@index')->name('additions.index');
                Route::get('/create','Dashboard\AdditionController@create')->name('additions.create');
                Route::post('/store','Dashboard\AdditionController@store')->name('additions.store');
                Route::get('/edit/{id}','Dashboard\AdditionController@edit')->name('additions.edit');
                Route::post('/update/{id}','Dashboard\AdditionController@update')->name('additions.update');
                Route::post('/{id}','Dashboard\AdditionController@destroy')->name('additions.destroy');
                Route::post('/status/{id}', 'Dashboard\AdditionController@updateStatus')->name('additions.status');
            });
            Route::prefix('advertisments')->group(function() {
                Route::get('','Dashboard\AdvertismentController@index')->name('advertisments.index');
                Route::get('/create','Dashboard\AdvertismentController@create')->name('advertisments.create');
                Route::post('/store','Dashboard\AdvertismentController@store')->name('advertisments.store');
                Route::get('/edit/{id}','Dashboard\AdvertismentController@edit')->name('advertisments.edit');
                Route::post('/update/{id}','Dashboard\AdvertismentController@update')->name('advertisments.update');
                Route::post('/{id}','Dashboard\AdvertismentController@destroy')->name('advertisments.destroy');
                Route::post('/status/{id}', 'Dashboard\AdvertismentController@updateStatus')->name('advertisments.status');
            });

            Route::prefix('category')->group(function() {
                Route::get('additions','Dashboard\CatAdditionsController@index')->name('category.additions.index');
                Route::get('additions/create','Dashboard\CatAdditionsController@create')->name('category.additions.create');
                Route::post('additions/store','Dashboard\CatAdditionsController@store')->name('category.additions.store');
                Route::get('additions/edit/{id}','Dashboard\CatAdditionsController@edit')->name('category.additions.edit');
                Route::post('additions/update/{id}','Dashboard\CatAdditionsController@update')->name('category.additions.update');
                Route::post('additions/{id}','Dashboard\CatAdditionsController@destroy')->name('category.additions.destroy');
            });

            Route::prefix('contacts')->group(function() {
                Route::get('','Dashboard\ContactsController@index')->name('contacts.index');
                Route::post('/{id}','Dashboard\ContactsController@destroy')->name('contacts.destroy');
            });
            Route::prefix('users')->group(function() {
                Route::get('','Dashboard\UsersController@index')->name('users.index');
                Route::get('/create','Dashboard\UsersController@create')->name('users.create');
                Route::post('/store','Dashboard\UsersController@store')->name('users.store');
                Route::get('/edit/{id}','Dashboard\UsersController@edit')->name('users.edit');
                Route::post('/update/{id}','Dashboard\UsersController@update')->name('users.update');
                Route::post('/{id}','Dashboard\UsersController@destroy')->name('users.destroy');
                Route::get('/export', 'Dashboard\UsersController@export')->name('users.export');
                Route::get('/show/{id}','Dashboard\UsersController@show')->name('users.show');
            });

            Route::prefix('stores')->group(function() {
                Route::get('','Dashboard\StoresController@index')->name('stores.index');
                Route::get('/create','Dashboard\StoresController@create')->name('stores.create');
                Route::post('/store','Dashboard\StoresController@store')->name('stores.store');
                Route::get('/edit/{id}','Dashboard\StoresController@edit')->name('stores.edit');
                Route::post('/update/{id}','Dashboard\StoresController@update')->name('stores.update');
                Route::post('/{id}','Dashboard\StoresController@destroy')->name('stores.destroy');
                Route::get('/export', 'Dashboard\StoresController@export')->name('stores.export');
                Route::post('/status/{id}', 'Dashboard\StoresController@updateStatus')->name('stores.status');
            });

            Route::prefix('products')->group(function() {
                Route::get('','Dashboard\ProductsController@index')->name('products.index');
                Route::get('/create','Dashboard\ProductsController@create')->name('products.create');
                Route::post('/store','Dashboard\ProductsController@store')->name('products.store');
                Route::get('/edit/{id}','Dashboard\ProductsController@edit')->name('products.edit');
                Route::post('/update/{id}','Dashboard\ProductsController@update')->name('products.update');
                Route::post('/{id}','Dashboard\ProductsController@destroy')->name('products.destroy');
                Route::get('show/{id}','Dashboard\ProductsController@show')->name('products.show');
                Route::post('/status/{id}', 'Dashboard\ProductsController@updateStatus')->name('products.status');
            });

            Route::prefix('products')->group(function() {
                Route::get('additions','Dashboard\ProductAdditionsController@index')->name('product.additions.index');
                Route::get('additions/create','Dashboard\ProductAdditionsController@create')->name('product.additions.create');
                Route::post('additions/store','Dashboard\ProductAdditionsController@store')->name('product.additions.store');
                Route::get('additions/edit/{id}','Dashboard\ProductAdditionsController@edit')->name('product.additions.edit');
                Route::post('additions/update/{id}','Dashboard\ProductAdditionsController@update')->name('product.additions.update');
                Route::post('additions/{id}','Dashboard\ProductAdditionsController@destroy')->name('product.additions.destroy');
            });
        });
        Route::group(['middleware' => ['role:Super Admin']], function () {

            Route::prefix('admins')->group(function() {
                Route::get('','Dashboard\AdminsController@index')->name('admins.index');
                Route::get('/create','Dashboard\AdminsController@create')->name('admins.create');
                Route::post('/store','Dashboard\AdminsController@store')->name('admins.store');
                Route::get('/edit/{id}','Dashboard\AdminsController@edit')->name('admins.edit');
                Route::post('/update/{id}','Dashboard\AdminsController@update')->name('admins.update');
                Route::post('/{id}','Dashboard\AdminsController@destroy')->name('admins.destroy');
            });

        });

    });

});
