<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
Route::prefix('home')->group(function () {
    Route::get('/', 'Api\HomeController@home');
    Route::get('getProductsFilter', 'Api\HomeController@getProductsFilter');
});
Route::prefix('settings')->group(function () {
    Route::get('getCitiesWithNeighborhoods', 'Api\CitiesController@getCitiesWithNeighborhoods');
    Route::get('aboutUs', 'Api\SettingsController@aboutUs');
    Route::get('terms', 'Api\SettingsController@terms');
    Route::get('getCategories', 'Api\SettingsController@getCategories');
    Route::post('sendContactUs', 'Api\SettingsController@sendContactUs');
});
Route::prefix('users')->group(function () {
    Route::post('register', 'Api\UsersController@register');
    Route::get('activeAccount/{token}', 'Api\UsersController@activeAccount');
    Route::post('login', 'Api\UsersController@login');
    Route::post('requestPasswordReset', 'Api\UsersController@requestPasswordReset');
    Route::post('getUserRelatedByResetCode', 'Api\UsersController@getUserRelatedByResetCode');
    Route::post('resetPassword', 'Api\UsersController@resetPassword')->middleware('auth:users');
    Route::middleware(['auth:users','verified'])->group(function () {
        Route::get('getProfile', 'Api\UsersController@getProfile');
        Route::post('updateProfile', 'Api\UsersController@updateProfile');
        Route::post('updatePassword', 'Api\UsersController@updatePassword');
        Route::post('logout', 'Api\UsersController@logout');
        Route::prefix('addresses')->group(function () {
            Route::post('addAddress', 'Api\UserAddressesController@addAddress');
            Route::put('updateAddress', 'Api\UserAddressesController@updateAddress');
            Route::delete('deleteAddress', 'Api\UserAddressesController@deleteAddress');
        });
    });
});


Route::prefix('stores')->group(function () {
    Route::get('getProducts', 'Api\StoresController@getProducts');
});


