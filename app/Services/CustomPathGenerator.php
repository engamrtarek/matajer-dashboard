<?php


namespace App\Services;

use App\Models\Advertising;
use App\Models\Category;
use App\Models\Product;
use App\Models\Store;
use App\Models\StoreProductAd;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Support\PathGenerator\PathGenerator;

class CustomPathGenerator implements PathGenerator
{
    public function getPath(Media $media) : string
    {
        if ($media->model_type == Category::class) {
            return 'categories/' . md5($media->id) . '/';
        }elseif ($media->model_type == Store::class){
            return 'stores/' . md5($media->id) . '/';
        }elseif ($media->model_type == Product::class){
            return 'products/' . md5($media->id) . '/';
        }elseif ($media->model_type == StoreProductAd::class){
            return 'stores_products_ads/' . md5($media->id) . '/';
        }elseif ($media->model_type == Advertising::class){
            return 'advertisement/' . md5($media->id) . '/';
        }
    }

    public function getPathForConversions(Media $media) : string
    {
        return $this->getPath($media) . 'conversions/';
    }

    public function getPathForResponsiveImages(Media $media): string
    {
        return $this->getPath($media) . 'responsive/';
    }
}
