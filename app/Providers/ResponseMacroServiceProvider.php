<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;
use Lang;
class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Register the application's response macros.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('api', function ($code, $message = [], $Data = [], $StatusResponse = 200) {
            $result = [];
            $result['code'] = $code;
            $result['msg'] = (is_array($message)) ? implode(',', $message) : Lang::get("api.$message");
            if ($code == 1) {
                $result['body'] = $Data;
            }
            return response()->json($result,$StatusResponse);
        });
    }
}
