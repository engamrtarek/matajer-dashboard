<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAddition extends Model
{
    protected $fillable = ['value','product_id','addition_id'];
    protected $appends  = ['addition_name_ar','addition_name_en'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function addition()
    {
        return $this->belongsTo(Addition::class, 'addition_id', 'id');
    }

    public function getAdditionNameArAttribute()
    {
        return $this->addition->getTranslation('name','ar');
    }

    public function getAdditionNameEnAttribute()
    {
        return $this->addition->getTranslation('name','en');
    }
}
