<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use HasApiTokens , Notifiable;
    protected $fillable = ['name','email','phone','password'];
    protected $hidden   = ['password', 'remember_token'];
    protected $casts    = ['email_verified_at' => 'datetime'];

    public function setPasswordAttribute($value)
    {
        if (!empty($value) && $value != null){
            $this->attributes['password'] = Hash::make($value);
        }
    }

    public function verifyUser()
    {
        return $this->hasOne(VerifyUser::class,'user_id','id');
    }

    public function addresses()
    {
        return $this->hasMany(UserAddress::class,'user_id','id');
    }

    public function productFavourites()
    {
        return $this->hasMany(UserProductFavourite::class,'user_id','id');
    }

    public function productReviews()
    {
        return $this->hasMany(UserProductReview::class,'user_id','id');
    }

}
