<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Neighborhood extends Model
{
    use HasTranslations;

    protected $fillable   = ['name','city_id'];
    public $translatable  = ['name'];
    protected $appends    = ['name_ar','name_en'];


    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function getNameArAttribute()
    {
        return $this->getTranslation('name','ar');
    }

    public function getNameEnAttribute()
    {
        return $this->getTranslation('name','en');
    }

}
