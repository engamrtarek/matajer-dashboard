<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;

class CategoryCopy extends Model
{
    use InteractsWithMedia;
    protected $table='categories';
    protected $fillable   = ['name','is_active'];
    public $translatable  = ['name'];
    protected $appends    = ['name_ar','name_en'];


    public function scopeActive($query)
    {
        return $query->whereIsActive(1);
    }

    public function stores()
    {
        return $this->hasMany(Store::class, 'category_id', 'id');
    }

    public function additions()
    {
        return $this->belongsToMany(Addition::class,'category_additions');
    }


    public function getNameArAttribute()
    {
        return $this->getTranslation('name','ar');
    }

    public function getNameEnAttribute()
    {
        return $this->getTranslation('name','en');
    }
}
