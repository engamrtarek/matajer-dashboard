<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Advertising extends Model  implements HasMedia
{
    use InteractsWithMedia;

    protected $table     = 'advertisement';
    protected $fillable  = ['link','is_active'];

    public function scopeActive($query)
    {
        return $query->whereIsActive(1);
    }
}
