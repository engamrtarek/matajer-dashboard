<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model implements HasMedia
{
    use SoftDeletes,InteractsWithMedia;
    protected $table='products';
    protected $fillable = ['name', 'desc', 'regular_price', 'sale_price', 'stock', 'is_approved', 'store_id'];
    protected $appends = ['on_sale', 'user_liked', 'avg_rate', 'num_users_rate'];

    public function scopeStockApproved($query)
    {
        return $query->where('stock','>',0)->whereIsApproved(1);
    }


    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id', 'id');
    }

    public function reviews()
    {
        return $this->hasMany(UserProductReview::class, 'product_id', 'id');
    }

    public function favourites()
    {
        return $this->hasMany(UserProductFavourite::class, 'product_id', 'id');
    }

    public function ads()
    {
        return $this->morphMany(StoreProductAd::class, 'ad');
    }

    public function additions()
    {
        return $this->hasMany(ProductAddition::class,'product_id','id');
    }


    public function getOnSaleAttribute()
    {
        return $this->sale_price != null ? true : false;
    }

    public function getAvgRateAttribute()
    {
        if ($this->reviews()->count()) {
            return round($this->reviews()->avg('rate'));
        }
        return 0;

    }

    public function getNumUsersRateAttribute()
    {
        return $this->reviews()->count();
    }

    public function getUserLikedAttribute()
    {
        $user = Auth::guard('users')->user();
        if ($user) {
            if ($user->productFavourites()->where('product_id', $this->id)->count()) {
                return true;
            }
            return false;
        }
        return false;
    }


}
