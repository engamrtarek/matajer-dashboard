<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdditionCopy extends Model
{
    protected $table='additions';
    protected $fillable   = ['name','is_active'];
    public $translatable  = ['name'];

    public function scopeActive($query)
    {
        return $query->whereIsActive(1);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class,'category_additions');
    }
    public function getNameArAttribute()
    {
        return $this->getTranslation('name','ar');
    }

    public function getNameEnAttribute()
    {
        return $this->getTranslation('name','en');
    }
}
