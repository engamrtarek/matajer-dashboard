<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NeighborhoodCopy extends Model
{
    protected $table='neighborhoods';
    protected $fillable   = ['name','city_id'];
    public $translatable  = ['name'];
    protected $appends    = ['name_ar','name_en'];


    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function getNameArAttribute()
    {
        return $this->getTranslation('name','ar');
    }

    public function getNameEnAttribute()
    {
        return $this->getTranslation('name','en');
    }

}
