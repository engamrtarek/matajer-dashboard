<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserProductReview extends Model
{
    protected $fillable   = ['review','rate','user_id','product_id'];

    public function getReviewAttribute($value)
    {
        if ($value != null){
            return $value;
        }
        return '';
    }

    public function getCreatedAtAttribute($key)
    {
        return Carbon::parse($key)->format('d-m-Y g:i A');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}
