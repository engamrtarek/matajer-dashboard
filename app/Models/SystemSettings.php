<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class SystemSettings extends Model
{
    use HasTranslations;

    protected $table     = 'system_settings' ;
    protected $fillable  = ['terms','about_us'];
    public $translatable = ['terms','about_us'];


}
