<?php

namespace App\Models;

use App\Http\Controllers\Api\ApiHelpersController;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class StoreProductAd extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $fillable = ['ad_type','ad_id','is_active'];


    public function scopeActive($query)
    {
        return $query->whereIsActive(1);
    }

    public function getAdTypeAttribute($value)
    {
        return $value == 'App\Models\Store' ? 1 : 2;
    }
}
