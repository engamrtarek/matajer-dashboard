<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use DB;
class Store extends Authenticatable implements HasMedia
{
    use HasApiTokens , Notifiable,InteractsWithMedia;
    protected $fillable = ['name','owner_name','email','phone','password','lat','lng','address',
                           'facebook_username','twitter_username','instagram_username','whatsapp_number',
                           'category_id','city_id','neighborhood_id','is_active'
                          ];
    protected $appends  = ['full_address'];
    protected $hidden   = ['password', 'remember_token'];
    protected $casts    = ['email_verified_at' => 'datetime'];

    public function scopeActive($query)
    {
        return $query->whereIsActive(1);
    }

    public function setPasswordAttribute($value)
    {
        if (!empty($value) && $value != null){
            $this->attributes['password'] = Hash::make($value);
        }
    }

    public function getFullAddressAttribute()
    {
        return $this->address.', '. $this->neighborhood->name.', '. $this->city->name;
    }

    public function getFacebookUsernameAttribute($value)
    {
        if ($value != null){
            return 'https://www.facebook.com/'.$value;
        }
        return '';
    }

    public function getTwitterUsernameAttribute($value)
    {
        if ($value != null){
            return 'https://twitter.com/'.$value;
        }
        return '';
    }

    public function getInstagramUsernameAttribute($value)
    {
        if ($value != null){
            return 'https://www.instagram.com/'.$value;
        }
        return '';
    }

    public function getWhatsappNumberAttribute($value)
    {
        if ($value != null){
            return $value;
        }
        return '';
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function neighborhood()
    {
        return $this->belongsTo(Neighborhood::class, 'neighborhood_id', 'id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'store_id', 'id');
    }

    public function ads()
    {
        return $this->morphMany(StoreProductAd::class, 'ad');
    }

    public static function getNearBy($lat,$lng,$distance = 5)
    {
        return self::Active()->select(['*', DB::raw('( 6371 * acos( cos( radians('.$lat.') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('.$lng.') ) + sin( radians('.$lat.') ) * sin( radians(lat) ) ) ) AS distance')])->having('distance', '<', $distance)->orderBy("distance");
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

}
