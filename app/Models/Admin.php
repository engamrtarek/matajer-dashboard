<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class Admin extends Authenticatable
{
    use Notifiable,HasRoles;
    protected $table='admins';
    protected $guard = 'admin';
    protected $primaryKey='id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $fillable = [
        'name', 'email', 'password','image'
    ];
    protected $hidden = [
        'password','email_verified_at','admins_created_at','admins_updated_at'
    ];
    public function setImageAttribute($value)
    {
        if ($value)
        {
            $fileName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('media/admin/'),$fileName);
            $this->attributes['image']=$fileName;
        }

    }
    public function getImageAttribute($value){
        if ($value) {
            return asset('media/admin/'.$value);
        } else {
            return asset('media/admin/default.png');
        }
    }
}
