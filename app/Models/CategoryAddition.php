<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryAddition extends Model
{
    protected $fillable   = ['category_id','addition_id'];
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
    public function addition()
    {
        return $this->belongsTo(Addition::class, 'addition_id');
    }
}
