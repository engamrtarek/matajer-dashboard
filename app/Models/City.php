<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class City extends Model
{
    use HasTranslations;

    protected $fillable   = ['name'];
    public $translatable  = ['name'];
    protected $appends    = ['name_ar','name_en'];


    public function neighborhoods()
    {
        return $this->hasMany(Neighborhood::class, 'city_id', 'id');
    }

    public function getNameArAttribute()
    {
        return $this->getTranslation('name','ar');
    }

    public function getNameEnAttribute()
    {
        return $this->getTranslation('name','en');
    }


}
