<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Api\ApiHelpersController;
use Closure;
use Auth;
use Request;

class Verified
{
    public function handle($request, Closure $next)
    {
        if (Auth::user()->email_verified_at == null) {
            return response()->api(0, 'usrWaitingActivation');
        }
        return $next($request);
    }
}
