<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Addition;
use App\Models\AdditionCopy;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class AdditionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $adds=Addition::orderBy('created_at','desc')->get();
        return view('pages.additions.index',compact('adds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.additions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            "en" => $request->name_en,
            "ar" =>$request->name_ar,
        ];
        $x=json_encode($data);
        AdditionCopy::create([
            "name" => $x,
        ]);
        Toastr::success('Additions added successfully!', 'Success', ["positionClass" => "toast-top-right"]);
        return redirect()->route('additions.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $add= Addition::findOrFail($id);
        return view('pages.additions.edit',compact('add'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $add=AdditionCopy::findOrFail($id);
        $data = [
            "en" => $request->name_en,
            "ar" =>$request->name_ar,
        ];
        $x=json_encode($data);
        $add->update([
            "name" => $x,
        ]);
        Toastr::success('Additions updated successfully!','Success',["positionClass" => "toast-top-right"]);
        return redirect()->route('additions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $add=Addition::find($id);
        $add->delete();
        Toastr::success('Additions deleted successfully!','Success',["positionClass" => "toast-top-right"]);
        return redirect()->back();
    }

    public function updateStatus(Request $request)
    {
        $add=Addition::findOrFail($request->id);
        $add->is_active = $request->is_active;
        if($add->save()){
            return 1;
        }
        return 0;
    }
}
