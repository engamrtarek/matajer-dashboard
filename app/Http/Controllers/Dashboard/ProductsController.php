<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\StoreRequest;
use App\Models\Product;
use App\Models\Store;
use App\Models\UserProductReview;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prods = Product::orderBy('created_at', 'desc')->get();
        return view('pages.products.index', compact('prods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stores=Store::all();
        return view('pages.products.create',compact('stores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $data = $request->validated();
        Product::create($data);
        Toastr::success('Product added successfully!', 'Success', ["positionClass" => "toast-top-right"]);
        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $prod = Product::findOrFail($id);
        $prodreview=UserProductReview::where('product_id',$prod->id)->get();
        return view('pages.products.show', compact('prod','prodreview'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $stores=Store::all();
        $prod = Product::findOrFail($id);
        return view('pages.products.edit', compact('prod','stores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $prod = Product::findOrFail($id);
        $prod->name=$request->name;
        $prod->desc=$request->desc;
        $prod->regular_price=$request->regular_price;
        $prod->sale_price=$request->sale_price;
        $prod->stock=$request->stock;
        $prod->store_id=$request->store_id;
        $prod->save();
        Toastr::success('product updated successfully!', 'Success', ["positionClass" => "toast-top-right"]);
        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prod = Product::findOrFail($id);
        $prod->delete();
        Toastr::success('product deleted!', 'Success', ["positionClass" => "toast-top-right"]);
        return redirect()->back();
    }

    public function updateStatus(Request $request)
    {
        $prod = Product::findOrFail($request->id);
        $prod->is_approved = $request->is_approved;
        if ($prod->save()) {
            return 1;
        }
        return 0;
    }
}
