<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\CityRequest;
use App\Models\City;
use App\Models\CityCopy;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities=City::orderBy('created_at','desc')->get();
        return view('pages.city.index',compact('cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.city.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            "en" => $request->name_en,
            "ar" =>$request->name_ar,
        ];
        $x=json_encode($data);
        CityCopy::create([
            "name" => $x,
        ]);
            Toastr::success('city added successfully!', 'Success', ["positionClass" => "toast-top-right"]);
            return redirect()->route('cities.index');

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city= City::findOrFail($id);
        return view('pages.city.edit',compact('city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $city=CityCopy::findOrFail($id);
        $data = [
            "en" => $request->name_en,
            "ar" =>$request->name_ar,
        ];
        $x=json_encode($data);
        $city->update([
            "name" => $x,
        ]);
        Toastr::success('city updated successfully!','Success',["positionClass" => "toast-top-right"]);
        return redirect()->route('cities.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $city = City::findOrFail($id);
        $city->delete();
        Toastr::success('city deleted successfully!','Success',["positionClass" => "toast-top-right"]);
        return redirect()->back();
    }

}
