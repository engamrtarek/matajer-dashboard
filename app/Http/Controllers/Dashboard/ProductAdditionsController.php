<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductAdditionRequest;
use App\Models\Addition;
use App\Models\Product;
use App\Models\ProductAddition;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class ProductAdditionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = ProductAddition::orderBy('created_at', 'desc')->get();
        return view('pages.product_additions.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $prods=Product::all();
        $adds=Addition::all();
        return view('pages.product_additions.create', compact('prods','adds'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductAdditionRequest $request)
    {
        $data = $request->validated();
        ProductAddition::create($data);
        Toastr::success('Product addition added successfully!', 'Success', ["positionClass" => "toast-top-right"]);
        return redirect()->route('product.additions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = ProductAddition::findOrFail($id);
        $prods=Product::all();
        $adds=Addition::all();
        return view('pages.product_additions.edit', compact('product','prods','adds'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = ProductAddition::findOrFail($id);
        $product->value=$request->value;
        $product->product_id=$request->product_id;
        $product->addition_id=$request->addition_id;
        $product->save();
        Toastr::success('product additions updated successfully!', 'Success', ["positionClass" => "toast-top-right"]);
        return redirect()->route('product.additions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = ProductAddition::findOrFail($id);
        $product->delete();
        Toastr::success('product additions deleted!', 'Success', ["positionClass" => "toast-top-right"]);
        return redirect()->back();
    }

}
