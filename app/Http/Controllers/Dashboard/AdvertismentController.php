<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdvertismentRequest;
use App\Models\Advertising;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class AdvertismentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $advertisments=Advertising::orderBy('created_at','desc')->get();
        return view('pages.advertisments.index',compact('advertisments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.advertisments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdvertismentRequest $request)
    {
        $data = $request->validated();
        Advertising::create($data);
        Toastr::success('advertising added successfully!', 'Success', ["positionClass" => "toast-top-right"]);
        return redirect()->route('advertisments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $advertisment = Advertising::findOrFail($id);
        return view('pages.advertisments.edit',compact('advertisment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdvertismentRequest $request, $id)
    {
        $advertisment = Advertising::findOrFail($id);
        $data = $request->validated();
        $advertisment->update($data);
        Toastr::success('advertisment updated successfully!','Success',["positionClass" => "toast-top-right"]);
        return redirect()->route('advertisments.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advertisment = Advertising::findOrFail($id);
        $advertisment->delete();
        Toastr::success('advertisment deleted!','Success',["positionClass" => "toast-top-right"]);
        return redirect()->back();
    }

    public function updateStatus(Request $request)
    {
        $cat=Advertising::findOrFail($request->id);
        $cat->is_active = $request->is_active;
        if($cat->save()){
            return 1;
        }
        return 0;
    }

}
