<?php

namespace App\Http\Controllers\Dashboard;

use App\Exports\StoresExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRequest;
use App\Models\Category;
use App\Models\City;
use App\Models\Neighborhood;
use App\Models\Store;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class StoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores=Store::orderBy('created_at','desc')->get();
        return view('pages.stores.index',compact('stores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats=Category::all();
        $cities=City::all();
        $neighborhoods=Neighborhood::all();
        return view('pages.stores.create',compact('cats','cities','neighborhoods'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $data= $request->validated();
        Store::create($data);
        Toastr::success('The store account has been created successfully!','Success',["positionClass" => "toast-top-right"]);
        return redirect()->route('stores.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $store=Store::find($id);
        $cats=Category::all();
        $cities=City::all();
        $neighborhoods=Neighborhood::all();
        return view('pages.stores.edit',compact('store','cats','cities','neighborhoods'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $store=Store::find($id);
        $store->name=$request->name;
        $store->owner_name=$request->owner_name;
        $store->email=$request->email;
        $store->phone=$request->phone;
        $store->lat=$request->lat;
        $store->lng=$request->lng;
        $store->address=$request->address;
        $store->facebook_username=$request->facebook_username;
        $store->twitter_username=$request->twitter_username;
        $store->instagram_username=$request->instagram_username;
        $store->whatsapp_number=$request->whatsapp_number;
        $store->category_id=$request->category_id;
        $store->city_id=$request->city_id;
        $store->neighborhood_id=$request->neighborhood_id;
        $store->save();
        Toastr::success('The store account has been updated successfully!','Success',["positionClass" => "toast-top-right"]);
        return redirect()->route('stores.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $store=Store::find($id);
        $store->delete();
        Toastr::success('Store successfully erased!','Success',["positionClass" => "toast-top-right"]);
        return redirect()->route('stores.index');
    }
    public function export()
    {
        return Excel::download(new StoresExport(), 'stores.xlsx');
    }

    public function updateStatus(Request $request)
    {
        $store=Store::findOrFail($request->id);
        $store->is_active = $request->is_active;
        if($store->save()){
            return 1;
        }
        return 0;
    }
}
