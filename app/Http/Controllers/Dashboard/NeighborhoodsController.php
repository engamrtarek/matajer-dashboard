<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Neighborhood;
use App\Models\NeighborhoodCopy;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class NeighborhoodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $neighborhoods=Neighborhood::orderBy('created_at','desc')->get();
        return view('pages.neighborhoods.index',compact('neighborhoods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities=City::all();
        return view('pages.neighborhoods.create',compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            "en" => $request->name_en,
            "ar" =>$request->name_ar,
        ];
        $x=json_encode($data);
        NeighborhoodCopy::create([
            "name" => $x,
            "city_id"=>$request->city_id
        ]);
        Toastr::success('neighborhood added successfully!', 'Success', ["positionClass" => "toast-top-right"]);
        return redirect()->route('neighborhoods.index');

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cities=City::all();
        $neighborhood= Neighborhood::findOrFail($id);
        return view('pages.neighborhoods.edit',compact('cities','neighborhood'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $neighborhood=NeighborhoodCopy::findOrFail($id);
        $data = [
            "en" => $request->name_en,
            "ar" =>$request->name_ar,
        ];
        $x=json_encode($data);
        $neighborhood->update([
            "name" => $x,
            "city_id"=>$request->city_id
        ]);
        Toastr::success('neighborhood updated successfully!','Success',["positionClass" => "toast-top-right"]);
        return redirect()->route('neighborhoods.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $neighborhood= Neighborhood::findOrFail($id);
        $neighborhood->delete();
        Toastr::success('neighborhood deleted successfully!','Success',["positionClass" => "toast-top-right"]);
        return redirect()->back();
    }

}
