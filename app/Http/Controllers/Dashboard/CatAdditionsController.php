<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\CatAdditionsRequest;
use App\Models\Addition;
use App\Models\Category;
use App\Models\CategoryAddition;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class CatAdditionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cats=CategoryAddition::orderBy('created_at','desc')->get();
        return view('pages.category_additions.index',compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats=Category::all();
        $adds=Addition::all();
        return view('pages.category_additions.create',compact('cats','adds'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CatAdditionsRequest $request)
    {
        $data = $request->validated();
        CategoryAddition::create($data);
        Toastr::success('category additions added successfully!', 'Success', ["positionClass" => "toast-top-right"]);
        return redirect()->route('category.additions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $catt = CategoryAddition::findOrFail($id);
        $cats=Category::all();
        $adds=Addition::all();
        return view('pages.category_additions.edit',compact('cats','adds','catt'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CatAdditionsRequest $request, $id)
    {
        $cat = CategoryAddition::findOrFail($id);
        $data = $request->validated();
        $cat->update($data);
        Toastr::success('category additions updated successfully!','Success',["positionClass" => "toast-top-right"]);
        return redirect()->route('category.additions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat = CategoryAddition::findOrFail($id);
        $cat->delete();
        Toastr::success('category additions deleted!','Success',["positionClass" => "toast-top-right"]);
        return redirect()->back();
    }
}
