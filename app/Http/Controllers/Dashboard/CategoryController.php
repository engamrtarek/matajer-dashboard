<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CategoryCopy;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cats=Category::orderBy('created_at','desc')->get();
        return view('pages.category.index',compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [
            "en" => $request->name_en,
            "ar" =>$request->name_ar,
        ];
        $x=json_encode($data);
        CategoryCopy::create([
            "name" => $x,
        ]);
        Toastr::success('category added successfully!', 'Success', ["positionClass" => "toast-top-right"]);
        return redirect()->route('categories.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat= Category::findOrFail($id);
        return view('pages.category.edit',compact('cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cat=CategoryCopy::findOrFail($id);
        $data = [
            "en" => $request->name_en,
            "ar" =>$request->name_ar,
        ];
        $x=json_encode($data);
        $cat->update([
            "name" => $x,
        ]);
        Toastr::success('category updated successfully!','Success',["positionClass" => "toast-top-right"]);
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat=Category::find($id);
        $cat->delete();
        Toastr::success('category deleted successfully!','Success',["positionClass" => "toast-top-right"]);
        return redirect()->back();
    }

    public function updateStatus(Request $request)
    {
        $cat=Category::findOrFail($request->id);
        $cat->is_active = $request->is_active;
        if($cat->save()){
            return 1;
        }
        return 0;
    }
}
