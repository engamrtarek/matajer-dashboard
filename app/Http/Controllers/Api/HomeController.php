<?php

namespace App\Http\Controllers\Api;

use App\Models\Advertising;
use App\Models\Category;
use App\Models\Product;
use App\Models\Store;
use App\Models\StoreProductAd;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use DB;
class HomeController extends ApiHelpersController {

    public function home(Request $request)
    {
        $rules = [
            'lat'   => ['nullable','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'lng'   => ['nullable','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->api(0,$validator->errors()->all());
        }
        $categories        = Category::Active()->whereHas('stores', function($q){$q->where('is_active',1);})->cursor();
        $storesProductsAds = StoreProductAd::Active()->cursor();
        $advertising       = Advertising::Active()->inRandomOrder()->first();
        $products          = Product::cursor();
        $home_list['stores_products_ads'] = [];   $home_list['categories']   = [];$home_list['new_products']        = [];
        $home_list['advertising']         = Null; $home_list['nears_stores'] = [];$home_list['top_rated_products']  = [];
        if ($storesProductsAds->count()) {
            foreach ($storesProductsAds as $k => $storesProductsAd) {
                $home_list['stores_products_ads'][$k]['id']            = $storesProductsAd->id;
                $home_list['stores_products_ads'][$k]['logo']          = $storesProductsAd->getFirstMediaUrl();
                $home_list['stores_products_ads'][$k]['type']          = $storesProductsAd->ad_type;
                $home_list['stores_products_ads'][$k]['store_model']   = $storesProductsAd->ad_type == 1 ? $this->returnStoreData(Store::find($storesProductsAd->ad_id)) : Null;
                $home_list['stores_products_ads'][$k]['product_model'] = $storesProductsAd->ad_type == 2 ? $this->returnProductData(Product::find($storesProductsAd->ad_id)) : Null;
            }
        }
        if ($categories->count()) {
            foreach ($categories as $k => $category) {
                $home_list['categories'][$k]['id']      = $category->id;
                $home_list['categories'][$k]['name_ar'] = $category->name_ar;
                $home_list['categories'][$k]['name_en'] = $category->name_en;
                $home_list['categories'][$k]['logo']    = $category->getFirstMediaUrl();
                $home_list['categories'][$k]['stores']  = [];
                foreach ($category->stores()->Active()->cursor() as $key => $store) {
                    array_push( $home_list['categories'][$k]['stores'], $this->returnStoreData($store,$request->lat,$request->lng));
                }
            }
        }
        if ($products->count()) {
            foreach ($products as $k => $product) {
                array_push($home_list['new_products'], $this->returnProductData($product));
            }
        }
        if ($advertising) {
                $home_list['advertising']['id']      = $advertising->id;
                $home_list['advertising']['img']     = $advertising->getFirstMediaUrl();
                $home_list['advertising']['link']    = $advertising->link;
        }
        if ($request->has('lat') && $request->has('lng')){
            //$nearStores = Store::getNearBy($request->lat,$request->lng)->cursor();
            $nearStores = Store::Active()->cursor();
            if ($nearStores->count()){
                foreach ($nearStores as $k => $store) {
                    array_push($home_list['nears_stores'], $this->returnStoreData($store,$request->lat,$request->lng));
                }
            }
        }
        if ($products->count()) {
            foreach ($products as $k => $product) {
                array_push($home_list['top_rated_products'], $this->returnProductData($product));
            }
        }
        return response()->api(1,'so',$home_list);
        }

    public function getProductsFilter(Request $request)
    {
        $rules = [
            'page'        => ['required', 'integer', 'min:1'],
            'sort_type'   => ['nullable', 'integer', 'between:0,2'],
            'search_text' => ['nullable', 'string'],
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->api(0, $validator->errors()->all());
        }
        $products = Product::StockApproved();
        if ($request->has('search_text') && (!(empty($request->search_text)) || $request->search_text != "")){
            $products = $products->where(function($q) use($request){
                $q->where('name','LIKE', "%{$request->search_text}%")->orWhere('desc','LIKE', "%{$request->search_text}%");
            });
        }
        if ($request->has('sort_type')) {
            if ($request->sort_type == 0) {
                $products = $products->has('reviews')->withCount(['reviews as average_rating' => function($query) {
                    $query->select(DB::raw('coalesce(avg(rate),0)'));
                }])->orderByDesc('average_rating');
            }elseif ($request->sort_type == 1) {
                $products = $products->orderByRaw(DB::raw('IFNULL(sale_price, regular_price)'));
            }else {
                $products = $products->orderByRaw(DB::raw('IFNULL(sale_price, regular_price) desc'));
            }
        }
        $data['pagination_num'] = 20;
        $data['products'] = [];
        foreach ($products->simplePaginate(20) as $k => $product) {
            array_push($data['products'], $this->returnProductData($product));
        }
        return response()->api(1, 'so', $data);
    }
}
