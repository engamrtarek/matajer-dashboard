<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;
use Lang;
use Illuminate\Support\Arr;
use Validator;

class ApiHelpersController extends Controller
{
    public function returnUserData($user)
    {
        $userArray = [];
        $userArray['id']               = $user->id;
        $userArray['name']             = $user->name;
        $userArray['email']            = $user->email;
        $userArray['phone']            = $user->phone;
        $userArray['verified']         = $user->email_verified_at != null ? true : false;
        $userArray['addresses'] = [];
        foreach ($user->addresses as  $k => $address) {
            $userArray['addresses'][$k]['id']              = $address->id;
            $userArray['addresses'][$k]['title']           = $address->title;
            $userArray['addresses'][$k]['apartment']       = $address->apartment;
            $userArray['addresses'][$k]['floor']           = $address->floor;
            $userArray['addresses'][$k]['building']        = $address->building;
            $userArray['addresses'][$k]['address']         = $address->address;
            $userArray['addresses'][$k]['city_id']         = $address->city_id;
            $userArray['addresses'][$k]['neighborhood_id'] = $address->neighborhood_id;
        }
        return $userArray;
    }

    public function returnStoreData($store,$lat = '' ,$lng = '')
    {
        $data = [];
        $data['id']             = $store->id;
        $data['name']           = $store->name;
        $data['owner_name']     = $store->owner_name;
        $data['email']          = $store->email;
        $data['phone']          = $store->phone;
        $data['logo']           = $store->getFirstMediaUrl('logo');
        $data['cover']          = $store->getFirstMediaUrl('cover');
        $data['full_address']   = $store->full_address;
        $data['distance']       = ($lat != '' && $lng != '' ? $this->calcDistance($store->lat,$store->lng,$lat,$lng) : null);
        $data['social']['facebook_username']  = $store->facebook_username;
        $data['social']['twitter_username']   = $store->twitter_username;
        $data['social']['instagram_username'] = $store->instagram_username;
        $data['social']['whatsapp_number']    = $store->whatsapp_number;
        return $data;
    }

    public function returnProductData($product)
    {
        $data = [];
        $data['id']              = $product->id;
        $data['name']            = $product->name;
        $data['desc']            = $product->desc;
        $data['regular_price']   = $product->regular_price;
        $data['sale_price']      = $product->sale_price != null ? $product->sale_price : 0;
        $data['on_sale']         = $product->on_sale;
        $data['stock']           = $product->stock;
        $data['is_favourite']    = $product->user_liked;
        $data['rate']            = $product->avg_rate;
        $data['num_users_rate']  = $product->num_users_rate;
        $data['images'] = [];$data['reviews'] = [];$data['additions'] = [];
        foreach ($product->getMedia() as $k => $media) {
            $data['images'][$k]['id']   = $media->id;
            $data['images'][$k]['img']  = $media->getFullUrl();
        }
        foreach ($product->reviews as $k => $review) {
            $data['reviews'][$k]['user_name']   = $review->user->name;
            $data['reviews'][$k]['review']      = $review->review;
            $data['reviews'][$k]['rate']        = $review->rate;
            $data['reviews'][$k]['created_at']  = $review->created_at;
        }
        foreach ($product->additions as $k => $addition) {
            $data['additions'][$k]['id']       = $addition->id;
            $data['additions'][$k]['value']    = $addition->value;
            $data['additions'][$k]['name_ar']  = $addition->addition_name_ar;
            $data['additions'][$k]['name_en']  = $addition->addition_name_en;
        }
        $data['store']['id']           = $product->store->id;
        $data['store']['name']         = $product->store->name;
        $data['store']['logo']         = $product->store->getFirstMediaUrl('logo');
        $data['store']['full_address'] = $product->store->full_address;
        return $data;
    }

    public function calcDistance($lat1,$lon1,$lat2,$lon2)
    {
        $R = 6371; // km
        $dLat = deg2rad($lat2-$lat1);
        $dLon = deg2rad($lon2-$lon1);
        $lat1 = deg2rad($lat1);
        $lat2 = deg2rad($lat2);

        $a = sin($dLat/2) * sin($dLat/2) + sin($dLon/2) * sin($dLon/2) * cos($lat1) * cos($lat2);
        $c = 2 * atan2(sqrt($a), sqrt(1-$a));
        return round($R * $c, 3);
    }
}
/*if ($request->hasFile('images')) {
            $fileAdders = Product::find(2)->addMultipleMediaFromRequest(['images'])->each(function ($fileAdder) {
                    $fileAdder->toMediaCollection();
                });
        }*/
