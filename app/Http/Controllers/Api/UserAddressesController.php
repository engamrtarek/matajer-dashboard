<?php

namespace App\Http\Controllers\Api;
use App\Models\Neighborhood;
use App\Models\User;
use App\Models\UserAddress;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Api\ApiHelpersController;
use Illuminate\Validation\Rule;
use Validator;
use Auth;

class UserAddressesController extends ApiHelpersController
{
    public function addAddress(Request $request)
    {
        if (Auth::user()->addresses()->count() >= 5) {
            return response()->api(0,'reachMaxAddress');
        }
        $rules = [
            'title'            => ['required'],
            'apartment'        => ['required'],
            'floor'            => ['required'],
            'building'         => ['required'],
            'address'          => ['required'],
            'city_id'          => ['required','integer','min:1','exists:cities,id'],
            'neighborhood_id'  => ['required','integer','min:1','exists:neighborhoods,id'],
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return response()->api(0,$validator->errors()->all());
        }
        if (Neighborhood::find($request->neighborhood_id)->city->id != $request->city_id) {
            return response()->api(0,'wrongParentCity');
        }
        Auth::user()->addresses()->create($request->all());
        return response()->api(1,'so',array_merge($this->returnUserData(Auth::user())));
    }

    public function updateAddress(Request $request)
    {
        $rules = [
            'address_id'       => ['required','integer','min:1','exists:user_addresses,id'],
            'title'            => ['required'],
            'apartment'        => ['required'],
            'floor'            => ['required'],
            'building'         => ['required'],
            'address'          => ['required'],
            'city_id'          => ['required','integer','min:1','exists:cities,id'],
            'neighborhood_id'  => ['required','integer','min:1','exists:neighborhoods,id'],
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return response()->api(0,$validator->errors()->all());
        }
        $address = UserAddress::find($request->address_id);
        if (Neighborhood::find($request->neighborhood_id)->city->id != $request->city_id) {
            return response()->api(0,'wrongParentCity');
        }
        if ($address->user_id != Auth::user()->id) {
            return response()->api(0,'addressNotToUser');
        }
        $address->update($request->only(['title','apartment','floor','building','address','city_id','neighborhood_id']));
        return response()->api(1,'so',array_merge($this->returnUserData(Auth::user())));
    }

    public function deleteAddress(Request $request)
    {
        $rules = [
            'address_id'       => ['required','integer','min:1','exists:user_addresses,id'],
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return response()->api(0,$validator->errors()->all());
        }
        $address = UserAddress::find($request->address_id);
        if ($address->user_id != Auth::user()->id) {
            return response()->api(0,'addressNotToUser');
        }
        $address->delete();
        return response()->api(1,'so',array_merge($this->returnUserData(Auth::user())));
    }
}
