<?php

namespace App\Http\Controllers\Api;

use App\Models\City;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Auth;
use Lang;

class CitiesController extends ApiHelpersController
{
    public function getCitiesWithNeighborhoods()
    {
        $citiesHaveNeighborhood = City::has('neighborhoods')->cursor();
        $data = [];
        foreach ($citiesHaveNeighborhood as $k => $city) {
            $data[$k]['id']        = $city->id;
            $data[$k]['name_ar']   = $city->name_ar;
            $data[$k]['name_en']   = $city->name_en;
            foreach ($city->neighborhoods as $key => $neighborhood) {
                $data[$k]['neighborhoods'][$key]['id']        = $neighborhood->id;
                $data[$k]['neighborhoods'][$key]['name_ar']   = $neighborhood->name_ar;
                $data[$k]['neighborhoods'][$key]['name_en']   = $neighborhood->name_en;
            }
        }
        return response()->api(1,'so',$data);
    }
}
