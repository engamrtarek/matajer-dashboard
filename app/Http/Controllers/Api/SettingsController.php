<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use App\Models\SystemSettings;
use App\Models\Contact;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;

class SettingsController extends ApiHelpersController
{
    public function aboutUs()
    {
        $aboutUs = SystemSettings::select('about_us')->first();
        $data = [
            'about_us_ar'   => $aboutUs->getTranslation('about_us','ar'),
            'about_us_en'   => $aboutUs->getTranslation('about_us','en')
        ];
        return response()->api(1,'so',$data);
    }

    public function terms()
    {
        $terms = SystemSettings::select('terms')->first();
        $data = [
            'terms_ar'   => $terms->getTranslation('terms','ar'),
            'terms_en'   => $terms->getTranslation('terms','en')
        ];
        return response()->api(1,'so',$data);
    }

    public function sendContactUs(Request $request)
    {
        $rules = [
            'name'         => ['required'],
            'email'        => ['required','email'],
            'phone'        => ['required','regex:/(^(0)(5)[0-9]{8}$)/u'],
            'subject'      => ['required'],
            'message'      => ['required'],
            'type'         => ['required','integer','between:0,1'],
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->api(0,$validator->errors()->all());
        }
         Contact::create($request->all());
        return response()->api(1,'contactSent',[]);
    }

    public function getCategories()
    {
        $categories  = Category::Active()->whereHas('stores', function($q){$q->where('is_active',1);})->cursor();
        $data = [];
        foreach ($categories as $k => $category) {
            $data[$k]['id']        = $category->id;
            $data[$k]['name_ar']   = $category->getTranslation('name','ar');
            $data[$k]['name_en']   = $category->getTranslation('name','en');
            $data[$k]['logo']      = $category->getFirstMediaUrl();
        }
        return response()->api(1,'so',$data);
    }


}
