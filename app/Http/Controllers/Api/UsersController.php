<?php

namespace App\Http\Controllers\Api;
use App\Mail\VerifyMail;
use App\Models\Neighborhood;
use App\Models\User;
use App\Models\VerifyUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Api\ApiHelpersController;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\Rule;
use Validator;
use Lang;
use Mail;
use View;
use Auth;
use DB;
use Illuminate\Support\Str;

class UsersController extends ApiHelpersController
{
    public function register(Request $request)
    {
        $rules = [
            'name'             => ['required','string'],
            'email'            => ['required','email',Rule::unique('users')],
            'phone'            => ['required',Rule::unique('users'),'regex:/(^(0)(5)[0-9]{8}$)/u'],
            'password'         => ['required','min:8'],
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->api(0,$validator->errors()->all());
        }
        DB::beginTransaction();
        try {
            $user = User::create($request->all());
            //$user->verifyUser()->create(['token' => sha1(time())]);
            //Mail::to($user->email)->send(new VerifyMail($user));
            DB::commit();
            return response()->api(1,'regSuccess');
            }catch (\Exception $ex) {
            DB::rollback();
            return response()->api(0, $ex->getMessage());
        }
    }
    public function activeAccount($token)
    {
        $verifyUser = VerifyUser::whereToken($token)->first();
        if($verifyUser){
            if($verifyUser->user->email_verified_at == null) {
                $verifyUser->user->email_verified_at = Carbon::now();
                $verifyUser->user->save();
                $verifyUser->delete();
                $status = "Your e-mail is verified. You can now login.";
            } else {
                $status = "Your e-mail is already verified. You can now login.";
            }
        } else {
            $status = "sorry your email cannot be identified";
        }
        return view('emails.activationDone',compact('status'));
    }
    public function login(Request $request)
    {
        $rules = [
            'email_or_phone'  => ['required'],
            'password'        => ['required','min:8'],
            'firebase_token'  => ['required'],
            'device_type'     => ['required','integer','between:0,1'],
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->api(0,$validator->errors()->all());
        }
        $col = (filter_var($request->email_or_phone, FILTER_VALIDATE_EMAIL)) ? 'email' : 'phone';
        if (Auth::attempt([$col => request('email_or_phone'),'password' => request('password')])) {
            $user = Auth::user();
            if ($user->email_verified_at != null) {
                $user->update(['firebase_token' => $request->firebase_token,'device_type' => $request->device_type]);
                return response()->api(1,'successLogin',array_merge($this->returnUserData($user),['token' => $user->createToken($user->name.'_login')->plainTextToken]));
            }else {
                Mail::to($user->email)->send(new VerifyMail($user));
                return response()->api(1,'usrWaitingActivation');
            }
        }
        return response()->api(0,'wrongCredential');
    }
    public function requestPasswordReset(Request $request)
    {
        $rules = [
            'email'  => ['required','email','exists:users,email'],
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->api(0,$validator->errors()->all());
        }
        $user = User::whereEmail($request->email)->first();
        $resetPassCode = substr(time(), 4);
        $checkIfHasResetBefore = DB::table('password_resets')->whereEmail($user->email)->first();
        if ($checkIfHasResetBefore) {
            DB::table('password_resets')->whereEmail($user->email)->update(['token' => $resetPassCode]);
        } else {
            DB::table('password_resets')->insert(['email' => $user->email,'token' => $resetPassCode]);
        }
        //Mail::to($user->email)->send(new ResetPassMail($user,$resetPassCode));
        return response()->api(1,'successResetSend',['code' => $resetPassCode]);
    }
    public function getUserRelatedByResetCode(Request $request)
    {
        $rules = [
            'email'        => ['required','email','exists:password_resets,email'],
            'reset_code'   => ['required','numeric','min:6','exists:password_resets,token'],
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->api(0,$validator->errors()->all());
        }
        $resetCode = DB::table('password_resets')->whereToken($request->reset_code)->whereEmail($request->email)->first();
        $user      = User::whereEmail($resetCode->email)->first();
        if (!$user) {
            return response()->api(0,'unf');
        }
        DB::table('password_resets')->whereToken($request->reset_code)->delete();
        return response()->api(1,'so',['token' => $user->createToken('user_'.$user->name.'_reset_password',['server:update_password'])->plainTextToken]);
    }
    public function resetPassword(Request $request)
    {
        $rules = [
            'new_password' => ['required','min:8'],
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->api(0,$validator->errors()->all());
        }
        $userToken = $request->user()->currentAccessToken();
        if ($userToken->abilities[0] != 'server:update_password'){
            return response()->api(0,'tokenNotAllowResetPass');
        }
        $request->merge(['password' => $request->new_password]);
        Auth::user()->update($request->only('password'));
        $userToken->delete();
        return response()->api(1,'passRstSuccess',[]);
    }
    public function getProfile()
    {
        $userData   = $this->returnUserData(Auth::user());
        $token      = Auth::user()->createToken('user_'.Auth::user()->name.'_get_profile')->plainTextToken;
        return response()->api(1,'so',array_merge($userData,['token' =>$token]));
    }
    public function updateDeviceToken(Request $request)
    {
        $rules = [
            'firebase_token' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->api(0,$validator->errors()->all());
        }
        Auth::user()->update(['firebase_token' => $request->firebase_token]);
        return response()->api(1,'so');
    }
    public function updatePassword(Request $request)
    {
        $rules = [
            'old_password' => ['required','min:8','old_password:'.Auth::user()->password],
            'new_password' => ['required','min:8','different:old_password'],
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->api(0,$validator->errors()->all());
        }
        $request->merge(['password' => $request->new_password]);
        Auth::user()->update($request->only('password'));
        return response()->api(1,'passwordUpdated');
    }
    public function updateProfile(Request $request)
    {
        $rules = [
            'phone'            => ['required',Rule::unique('users')->ignore(Auth::user()->id),'regex:/(^(0)(5)[0-9]{8}$)/u'],
            'device_type'      => ['nullable','integer','between:0,1'],
            'firebase_token'   => ['nullable','string'],
        ];
        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails()) {
            return response()->api(0,$validator->errors()->all());
        }
        Auth::user()->update($request->only(['phone','device_type','firebase_token']));
        $userData   = $this->returnUserData(Auth::user());
        $token      = Auth::user()->createToken('user_'.Auth::user()->name.'_get_profile')->plainTextToken;
        return response()->api(1,'so',array_merge($userData,['token' =>$token]));
    }
    public function logout()
    {
        Auth::user()->currentAccessToken()->delete();
        Auth::user()->update(['firebase_token' => null]);
        return response()->api(1,'logOutSuccess');
    }

}
