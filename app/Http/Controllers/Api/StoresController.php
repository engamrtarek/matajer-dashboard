<?php

namespace App\Http\Controllers\Api;

use App\Models\Store;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Api\ApiHelpersController;
use Illuminate\Validation\Rule;
use Validator;
use Auth;
use Illuminate\Support\Str;
use DB;

class StoresController extends ApiHelpersController
{
    public function getProducts(Request $request)
    {
        $rules = [
            'store_id'    => ['required', 'integer', 'min:1', 'exists:stores,id'],
            'page'        => ['required', 'integer', 'min:1'],
            'sort_type'   => ['nullable', 'integer', 'between:0,2'],
            'search_text' => ['nullable', 'string'],

        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->api(0, $validator->errors()->all());
        }
        $store = Store::find($request->store_id);
        if ($store->is_active != 1) {
            return response()->api(0, 'storeNotActive');
        }
        $storeProducts = $store->products()->StockApproved();
        if ($request->has('search_text') && (!(empty($request->search_text)) || $request->search_text != "")){
            $storeProducts = $storeProducts->where(function($q) use($request){
                $q->where('name','LIKE', "%{$request->search_text}%")->orWhere('desc','LIKE', "%{$request->search_text}%");
            });
        }
        if ($request->has('sort_type')) {
            if ($request->sort_type == 0) {
                $storeProducts = $storeProducts->has('reviews')->withCount(['reviews as average_rating' => function($query) {
                    $query->select(DB::raw('coalesce(avg(rate),0)'));
                }])->orderByDesc('average_rating');
            }elseif ($request->sort_type == 1) {
                $storeProducts = $storeProducts->orderByRaw(DB::raw('IFNULL(sale_price, regular_price)'));
            }else {
                $storeProducts = $storeProducts->orderByRaw(DB::raw('IFNULL(sale_price, regular_price) desc'));
            }
        }
        $data['pagination_num'] = 20;
        $data['products'] = [];
        foreach ($storeProducts->simplePaginate(20) as $k => $product) {
            array_push($data['products'], $this->returnProductData($product));
        }
        return response()->api(1, 'so', $data);

    }
}
