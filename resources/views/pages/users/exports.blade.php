<table>
    <thead>
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Email</th>
        <th>email verified at</th>
        <th>phone</th>
        <th>created at</th>
    </tr>
    </thead>


    <tbody>
    @foreach($users as $key=>$user)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>@if(isset($user->email_verified_at))
                    {{$user->email_verified_at}}
                @else
                    user not verified yet
                @endif</td>
            <td>{{$user->phone}}</td>
            <td>{{$user->created_at}}</td>
        </tr>
    @endforeach
    </tbody>

</table>
