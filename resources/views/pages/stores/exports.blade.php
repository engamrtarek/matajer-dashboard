<table>
    <thead>
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Owner Name</th>
        <th>Email</th>
        <th>phone</th>
        <th>address</th>
        <th>created at</th>
    </tr>
    </thead>


    <tbody>
    @foreach($stores as $key=>$store)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$store->name}}</td>
            <td>{{$store->owner_name}}</td>
            <td>{{$store->email}}</td>
            <td>{{$store->phone}}</td>
            <td>{{$store->city->name}} - {{$store->neighborhood->name}} - {{$store->address}}</td>
            <td>{{$store->created_at}}</td>

        </tr>
    @endforeach
    </tbody>

</table>
