<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 3; $i++) {
            \App\Models\User::create([
                'name' => Str::random(8),
                'email' => Str::random(12).'@mail.com',
                'password' => bcrypt('123456'),
                'phone'=>mt_rand(10000000000, 99999999999)
            ]);

        }
    }
}
