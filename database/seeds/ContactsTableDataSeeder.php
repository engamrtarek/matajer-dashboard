<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ContactsTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 3; $i++) {
            \App\Models\Contact::create([
                'name' =>  Str::random(8),
                'subject' =>  Str::random(8),
                'email' =>  Str::random(12).'@mail.com',
                'phone'=>mt_rand(10000000000, 99999999999),
                'message'=>  Str::random(8),
                'type'=>0,
            ]);
        }
    }
}
